﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    public Vector2 move;
    public Vector2 velocity;
    public float maxSpeed = 5.0f;


    // Update is called once per frame
    void Update() {

        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");


        Vector2 velocity = direction * maxSpeed;

        //scale the velocity by frame duration
      
     Vector2 move = velocity * Time.deltaTime;
        //move the object
        transform.Translate(move);

        transform.Translate(velocity * Time.deltaTime);
    }
}
